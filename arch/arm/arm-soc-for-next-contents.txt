arm/soc
	patch
		ARM: ep93xx: remove MaverickCrunch support
	at91/soc
		git://git.kernel.org/pub/scm/linux/kernel/git/at91/linux tags/at91-soc-5.15
	omap/soc
		git://git.kernel.org/pub/scm/linux/kernel/git/tmlind/linux-omap tags/omap-for-v5.15/soc-signed
	omap/soc-late
		git://git.kernel.org/pub/scm/linux/kernel/git/tmlind/linux-omap tags/omap-for-v5.15/soc-late-signed

arm/dt
	patch
		ARM: dts: owl-s500: Add ethernet support
		ARM: dts: owl-s500-roseapplepi: Add ethernet support
	renesas/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/geert/renesas-devel tags/renesas-arm-dt-for-v5.15-tag1
	renesas/dt-bindings
		git://git.kernel.org/pub/scm/linux/kernel/git/geert/renesas-devel tags/renesas-dt-bindings-for-v5.15-tag1
	ux500/dts
		git://git.kernel.org/pub/scm/linux/kernel/git/linusw/linux-nomadik tags/ux500-dts-v5.15-1
	at91/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/at91/linux tags/at91-dt-5.15
	omap/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/tmlind/linux-omap tags/omap-for-v5.15/dt-signed
	sti/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/pchotard/sti tags/sti-dt-for-v5.15-round1
	stm32/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/atorgue/stm32 tags/stm32-dt-for-v5.15-1
	ixp4xx/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/linusw/linux-nomadik tags/ixp4xx-dts-arm-soc-v5.15-1
	mediatek/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/matthias.bgg/linux tags/v5.14-next-dts32
	mediatek/dt64
		git://git.kernel.org/pub/scm/linux/kernel/git/matthias.bgg/linux tags/v5.14-next-dts64
	k3/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/nmenon/linux tags/ti-k3-dt-for-v5.15
	amlogic/dt64
		git://git.kernel.org/pub/scm/linux/kernel/git/amlogic/linux tags/amlogic-arm64-dt-for-v5.15
	amlogic/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/amlogic/linux tags/amlogic-arm-dt-for-v5.15
	samsung/dt
		git://git.kernel.org/pub/scm/linux/kernel/git/krzk/linux tags/samsung-dt-5.15
	samsung/dt64
		git://git.kernel.org/pub/scm/linux/kernel/git/krzk/linux tags/samsung-dt64-5.15
	omap/cpsw-dt
		git://git.kernel.org/pub/scm/linux/kernel/git/tmlind/linux-omap tags/omap-for-v5.15/dt-am3-signed

arm/drivers
	renesas/driver
		git://git.kernel.org/pub/scm/linux/kernel/git/geert/renesas-devel tags/renesas-drivers-for-v5.15-tag1
	ixp4xx/drivers
		git://git.kernel.org/pub/scm/linux/kernel/git/linusw/linux-nomadik tags/ixp4xx-drivers-arm-soc-v5.15-1
	omap/ti-sysc
		git://git.kernel.org/pub/scm/linux/kernel/git/tmlind/linux-omap tags/omap-for-v5.15/ti-sysc-signed
	patch
		bus: ixp4xx: return on error in ixp4xx_exp_probe()
	imx/ecspi
		git://git.kernel.org/pub/scm/linux/kernel/git/shawnguo/linux tags/imx-ecspi-5.15
	mediatek/drivers
		git://git.kernel.org/pub/scm/linux/kernel/git/matthias.bgg/linux tags/v5.14-next-soc
	drivers/memory
		git://git.kernel.org/pub/scm/linux/kernel/git/krzk/linux-mem-ctrl tags/memory-controller-drv-5.15
	keystone/drivers
		git://git.kernel.org/pub/scm/linux/kernel/git/ssantosh/linux-keystone tags/drivers_soc_for_5.15
	drivers/scmi
		git://git.kernel.org/pub/scm/linux/kernel/git/sudeep.holla/linux tags/scmi-updates-5.15
	omap/smartreflex
		git://git.kernel.org/pub/scm/linux/kernel/git/tmlind/linux-omap tags/omap-for-v5.15/sr-signed

arm/defconfig
	at91/defconfig
		git://git.kernel.org/pub/scm/linux/kernel/git/at91/linux tags/at91-defconfig-5.15
	amlogic/defconfig
		git://git.kernel.org/pub/scm/linux/kernel/git/amlogic/linux tags/amlogic-arm-configs-for-v5.15

arm/newsoc

arm/late

arm/fixes
	patch
		ARM: configs: Update the nhk8815_defconfig
		ARM: ixp4xx: fix building both pci drivers

