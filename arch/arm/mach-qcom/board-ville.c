// SPDX-License-Identifier: GPL-2.0-only

#include <linux/ioport.h>
#include <linux/platform_device.h>
#include <asm/mach/arch.h>

static const char *const ville_dt_match[] __initconst = {
	"htc,ville",
	NULL,
};

static struct resource ram_console_resources[] = {
	{
		.flags = IORESOURCE_MEM
	},
};

static struct platform_device ram_console_device = {
	.name		= "ram_console",
	.id		= -1,
	.num_resources	= ARRAY_SIZE(ram_console_resources),
	.resource	= ram_console_resources,
};

void __init ville_ram_console_debug_init_mem(unsigned long start, unsigned long size)
{
	struct resource *res;

	res = platform_get_resource(&ram_console_device, IORESOURCE_MEM, 0);
	if (!res)
		goto fail;
	res->start = start;
	res->end = res->start + size - 1;

	return;

fail:
	ram_console_device.resource = NULL;
	ram_console_device.num_resources = 0;
	pr_err("Failed to reserve memory block for ram console\n");
}

void __init ville_ram_console_debug_init(void)
{
	int err;

	err = platform_device_register(&ram_console_device);
	if (err) {
		pr_err("%s: ram console registration failed (%d)!\n", __func__, err);
	}
}

static void __init ville_early_init(void)
{
	ville_ram_console_debug_init_mem(0x88900000, 0xe0000);
	ville_ram_console_debug_init();
}

DT_MACHINE_START(VILLE, "ville")
	.dt_compat = ville_dt_match,
	.init_early = ville_early_init,
MACHINE_END
