#!/bin/sh
#pmbootstrap chroot -- apk add abootimg android-tools mkbootimg dtbtool
pmbootstrap chroot -- sh -c "
cat /mnt/linux/.output/arch/arm/boot/zImage /mnt/linux/.output/arch/arm/boot/dts/qcom-msm8960-htc-ville.dtb >/mnt/linux/.output/zImage-dtb;
abootimg -u /mnt/linux/boot.img -k /mnt/linux/.output/zImage-dtb;
